import pytest
from rest_framework.test import APIClient
from backend.models import Label, Store


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def create_store(db):
    def make_store(**kwargs):
        store = Store.objects.create(**kwargs)

        return store

    return make_store


@pytest.fixture
def create_label(db):
    def make_label(**kwargs):
        label = Label.objects.create(**kwargs)

        return label

    return make_label
