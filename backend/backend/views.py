from backend.serializers import StoreSerializer, LabelSerializer
from backend.models import Store, Label
from rest_framework import generics
from rest_framework import exceptions
from urllib import parse
import re


class StoreList(generics.ListCreateAPIView):
    """Handles get and post methods.

    If method is 'get' all stores are returned, if method is 'post'
    a new store is added.
    """
    queryset = Store.objects.all()
    serializer_class = StoreSerializer


class StoreDetail(generics.RetrieveAPIView):
    """Get a specific Store by id.
    """
    queryset = Store.objects.all()
    serializer_class = StoreSerializer


class LabelList(generics.ListCreateAPIView):
    queryset = Label.objects.all()
    serializer_class = LabelSerializer


class LabelDetail(generics.RetrieveAPIView):
    queryset = Label.objects.all()
    serializer_class = LabelSerializer


class StoreByLabel(generics.ListAPIView):
    serializer_class = StoreSerializer

    def is_query_string_labels_valid(self, qs):
        """Validate a query string with label's id.

        This query string must contain integers separated by commas.

        Args:
            qs (str): the query string to validate.

        Returns:
            bool: True if the string is valid, False otherwise.
        """
        regex = re.compile(r"^\d+(,\d+)*$")
        match = regex.search(qs)

        return True if match else False

    def get_queryset(self):
        data = self.request.query_params.get('data')
        if data:
            data = parse.unquote(data)
            if self.is_query_string_labels_valid(data):
                labels = data.split(',')
                queryset = Store.objects.filter(label__id__in=labels)
                if queryset:
                    return queryset
                raise exceptions.NotFound(
                    detail='No existen tiendas para las etiquetas dadas.')
            else:
                raise exceptions.ParseError(
                    'El query string no tiene el formato correcto.')
