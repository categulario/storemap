# from rest_framework import serializers
from backend.models import Store, Label
from rest_framework_gis import serializers
from rest_framework import serializers as s


class StoreSerializer(serializers.GeoFeatureModelSerializer):
    labels = s.SerializerMethodField('get_store_labels')

    def get_store_labels(self, instance):
        labels = Label.objects.filter(store__id=instance.id).values(
            'id', 'name')

        return LabelSerializer(labels, many=True, context=self.context).data

    class Meta:
        model = Store
        geo_field = 'location'
        fields = ('id', 'name', 'address', 'location', 'contact_name',
                  'phone_number', 'email', 'url', 'opening_hour',
                  'closing_hour', 'has_delivery', 'labels')


class LabelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Label
        fields = ('id', 'name', 'description')
