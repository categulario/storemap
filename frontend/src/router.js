import Vue from 'vue';
import VueRouter from 'vue-router';

// pages
import MainContainerComponent from '@/components/MainContainerComponent.vue';
import MapPage from '@/components/pages/MapPage.vue';
import RegisterPage from '@/components/pages/RegisterPage.vue';
import AboutPage from '@/components/pages/AboutPage.vue';

const routes = [
  {
    path: '/',
    component: MainContainerComponent,
    children: [
      { path: '', name: 'mapPage', component: MapPage },
      { path: 'register', name: 'registerPage', component: RegisterPage },
      { path: 'about', name: 'aboutPage', component: AboutPage },
    ],
  },
];

Vue.use(VueRouter);

const router = new VueRouter({
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  mode: 'history',
  routes,
});

export default router;
