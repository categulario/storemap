import Vue from 'vue';
import store from '@/store';
import router from '@/router';

import axios from 'axios';

import App from '@/App.vue';
import './registerServiceWorker';
import { Map, TileLayer, OsmSource } from 'vuelayers';
import 'vuelayers/lib/style.css';

Vue.use(Map);
Vue.use(TileLayer);
Vue.use(OsmSource);

axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.xsrfHeaderName = 'X-CSRFToken';

Vue.config.productionTip = false;

new Vue({
  router,
  store,

  render: (h) => h(App),
}).$mount('#app');
